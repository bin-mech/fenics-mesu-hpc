#!/bin/bash
source env-build-fenics.sh

mkdir -p ${BUILD_DIR}

cd ${BUILD_DIR} && \
    wget --read-timeout=10 -nc http://www.vtk.org/files/release/8.1/VTK-8.1.1.tar.gz 
    tar -xf VTK-8.1.1.tar.gz  && \
    cd VTK-8.1.1 && \
    mkdir -p build && \
    cd build && \
    cmake ../  -DCMAKE_BUILD_TYPE=Release \
               -DCMAKE_INSTALL_PREFIX=${PREFIX} \
               -DVTK_PYTHON_VERSION=${FENICS_PYTHON_VERSION} \
               -DVTK_WRAP_PYTHON=ON \
               -DModule_vtkIOXdmf3=ON \
               -DVTK_Group_MPI=ON && \
    make -j ${BUILD_THREADS} && \
    make install && \
    cd Wrapping/Python && \
    make -j ${BUILD_THREADS} && \
    make install 
   

