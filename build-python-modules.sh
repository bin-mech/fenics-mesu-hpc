#!/bin/bash
source env-build-fenics.sh
${FENICS_PYTHON} -m venv ${PREFIX}
source ${PREFIX}/bin/activate
echo $PYTHONPATH
echo $PREFIX
export PETSC_DIR=${PREFIX}
export SLEPC_DIR=${PREFIX}

MPI4PY_VERSION=3.0.3
PETSC4PY_VERSION=3.12.0
SLEPC4PY_VERSION=3.12.0

cd ${BUILD_DIR} && \
	wget --read-timeout=10 -nc https://bootstrap.pypa.io/get-pip.py && \
	${FENICS_PYTHON} get-pip.py  && \
	${FENICS_PYTHON} -m pip install --upgrade pip && \
	cd ${PREFIX}

cd ${BUILD_DIR}
${FENICS_PYTHON} -m pip install --timeout 100 --no-cache-dir matplotlib numpy numexpr ply six sympy && \
${FENICS_PYTHON} -m pip install --timeout 100 --no-cache-dir https://bitbucket.org/mpi4py/mpi4py/downloads/mpi4py-${MPI4PY_VERSION}.tar.gz && \
${FENICS_PYTHON} -m pip install --timeout 100 --no-cache-dir https://bitbucket.org/petsc/petsc4py/downloads/petsc4py-${PETSC4PY_VERSION}.tar.gz && \
${FENICS_PYTHON} -m pip install --timeout 100 --no-cache-dir https://bitbucket.org/slepc/slepc4py/downloads/slepc4py-${SLEPC4PY_VERSION}.tar.gz && \
${FENICS_PYTHON} -m pip install --timeout 100 --no-cache-dir pandas ipython ipyparallel h5py jupyter meshio pygmsh jupyterlab memory-profiler matplotlib json5 autopep8 numpy

