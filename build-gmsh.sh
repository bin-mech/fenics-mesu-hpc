#!/bin/bash
source env-build-fenics.sh

GMSH_VERSION="4.5.2"

mkdir -p $BUILD_DIR

## For some unknown reason, the gmsh install in /usr/bin does not work on all nodes.
## So we do a quick installation
cd ${BUILD_DIR} && \
   wget --read-timeout=10 -nc http://gmsh.info/bin/Linux/gmsh-${GMSH_VERSION}-Linux64.tgz && \
   tar -xzf gmsh-${GMSH_VERSION}-Linux64.tgz  
   cd  gmsh-${GMSH_VERSION}-Linux64 && \
   cp  ${BUILD_DIR}/gmsh-${GMSH_VERSION}-Linux64/bin/gmsh   ${PREFIX}/bin 
   cp  ${BUILD_DIR}/gmsh-${GMSH_VERSION}-Linux64/bin/onelab.py   ${PREFIX}/bin 
   cp -rf ${BUILD_DIR}/gmsh-${GMSH_VERSION}-Linux64/share/doc/gmsh   ${PREFIX}/share/doc/ 
   mkdir -p ${PREFIX}/share/man/man1
   cp  ${BUILD_DIR}/gmsh-${GMSH_VERSION}-Linux64/share/man/man1/gmsh.1  ${PREFIX}/share/man/man1/ 
   echo "finish the quick installation of gmsh"
 		

