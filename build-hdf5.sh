#!/bin/bash
source env-build-fenics.sh

SZIP_VERSION="2.1.1"
ZLIB_VERSION="1.2.11"
HDF5_VERSION="1.10.1"

mkdir -p $BUILD_DIR

cd ${BUILD_DIR} && \
   wget --read-timeout=10 -nc https://support.hdfgroup.org/ftp/lib-external/szip/2.1.1/src/szip-${SZIP_VERSION}.tar.gz 
   tar -xf szip-${SZIP_VERSION}.tar.gz && \
   cd szip-${SZIP_VERSION} && \
   ./configure --prefix=${PREFIX} && \
   make -j ${BUILD_THREADS} && \
   make check && \
   make install

sleep 5   

cd ${BUILD_DIR} && \
   wget --read-timeout=10 -nc http://www.zlib.net/zlib-${ZLIB_VERSION}.tar.gz
   tar -xf zlib-${ZLIB_VERSION}.tar.gz && \
   cd zlib-${ZLIB_VERSION} && \
   ./configure --prefix=${PREFIX} && \
   make -j ${BUILD_THREADS} && \
   make test && \
   make install

sleep 5   

cd ${BUILD_DIR} && \
   wget --read-timeout=10 -nc https://www.hdfgroup.org/package/source-bzip2/?wpdmdl=4300 -O hdf5-${HDF5_VERSION}.tar.bz2 
   tar -xf hdf5-${HDF5_VERSION}.tar.bz2 && \
   cd hdf5-${HDF5_VERSION} && \
   mkdir -p build && \
   cd build && \
   CC=mpicc ../configure --enable-parallel  \
                         --prefix=${PREFIX} \
                         --with-zlib=${PREFIX} \
                         --with-szlib=${PREFIX} \
                         --with-default-api-version=v110 \
                         --enable-build-mode=production && \
   make -j ${BUILD_THREADS} && \
   make install                    
