#!/bin/bash
source env-build-fenics.sh

export PETSC_DIR=${PREFIX}
export SLEPC_DIR=${PREFIX}
export BOOST_ROOT=${PREFIX}
export PYBIND11_DIR=${PREFIX}
FENICS_VERSION="cecile/mixed-dimensional" #"2017.2.0"
#FENICS_VERSION="master"
FFC_VERSION="master"

mkdir -p $BUILD_DIR

cd $BUILD_DIR && \
    if [ ! -d "ffc" ]; then
        git clone https://bitbucket.org/fenics-project/ffc.git
    fi && \
    cd ffc && \
    git checkout ${FFC_VERSION} && \
    ${FENICS_PYTHON} setup.py install

sleep 1

cd $BUILD_DIR && \
    if [ ! -d "dijitso" ]; then
        git clone https://bitbucket.org/fenics-project/dijitso.git
    fi && \
    cd dijitso && \
    git checkout ${FFC_VERSION} && \
    ${FENICS_PYTHON} setup.py install

sleep 1

cd $BUILD_DIR && \
    if [ ! -d "ufl" ]; then
        git clone https://bitbucket.org/fenics-project/ufl.git
    fi && \
    cd ufl && \
    git checkout ${FFC_VERSION} && \
    ${FENICS_PYTHON} setup.py install

sleep 1

cd $BUILD_DIR && \
    if [ ! -d "fiat" ]; then
        git clone https://bitbucket.org/fenics-project/fiat.git
    fi && \
    cd fiat && \
    git checkout ${FENICS_VERSION} && \
    ${FENICS_PYTHON} setup.py install

sleep 1

USE_PYTHON3=$(${FENICS_PYTHON} -c "import sys; print('OFF' if sys.version_info.major == 2 else 'ON')")
cd $BUILD_DIR && \
    if [ ! -d "dolfin" ]; then
        git clone https://bitbucket.org/fenics-project/dolfin.git
    fi && \
    cd dolfin && \
    git checkout ${FENICS_VERSION} && \
    mkdir -p build && \
    cd build && \
    cmake ../   -DDOLFIN_ENABLE_DOCS=False \
                -DHDF5_PREFER_PARALLEL=True \
                -DHDF5_ROOT=${PREFIX} \
                -DHDF5_C_HL_LIBRARIES=${PREFIX}/lib \
                -DHDF5_INCLUDE_DIRS=${PREFIX}/include \
                -DHDF5_C_LIBRARIES=${PREFIX}/lib \
                -DZLIB_ROOT=${PREFIX} \
                -DBOOST_ROOT=${PREFIX} \
                -DDOLFIN_ENABLE_HDF5=True \
                -DDOLFIN_ENABLE_VTK=True \
                -DDOLFIN_ENABLE_OPENMP=False \
                -DDOLFIN_ENABLE_SCOTCH=True \
                -DDOLFIN_ENABLE_SPHINX=False \
                -DDOLFIN_ENABLE_TRILINOS=False \
                -DDOLFIN_SKIP_BUILD_TESTS=True \
                -DCMAKE_BUILD_TYPE=Release \
                -DSLEPC_INCLUDE_DIRS=${PREFIX}/include \
                -DPETSC_INCLUDE_DIRS=${PREFIX}/include \
                -DSWIG_EXECUTABLE:FILEPATH=${PREFIX}/bin/swig \
                -DEIGEN3_INCLUDE_DIR:FILEPATH=${PREFIX}/include/eigen3 \
                -DCMAKE_INSTALL_PREFIX=${PREFIX} \
                -DDOLFIN_USE_PYTHON3=${USE_PYTHON3} \
                -DPYTHON_EXECUTABLE:FILEPATH=$(which ${FENICS_PYTHON}) && \
    make -j ${BUILD_THREADS} && \
    make install
    cd ../python
    $FENICS_PYTHON setup.py install --prefix=${PREFIX}


