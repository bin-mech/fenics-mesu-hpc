#!/bin/bash
# chmod u+x build-python.sh
source env-build-fenics.sh

PYTHON_VERSION="3.7.3"

mkdir -p $BUILD_DIR

cd ${BUILD_DIR} && \
   wget --read-timeout=10 -nc https://www.python.org/ftp/python/${PYTHON_VERSION}/Python-${PYTHON_VERSION}.tar.xz
   mkdir -p ${BUILD_DIR}/python && \
   tar -xf Python-${PYTHON_VERSION}.tar.xz -C ${BUILD_DIR}/python --strip-components=1 && \
   cd python && \
   ./configure --prefix=${PREFIX} && \
   make -j ${BUILD_THREADS} && \
   #make test && \
   make install && \
   
cd ${BUILD_DIR} && \
wget --read-timeout=10 -nc https://bootstrap.pypa.io/get-pip.py && \
${FENICS_PYTHON} get-pip.py --user && \
${FENICS_PYTHON} -m pip install --upgrade pip && \
cd ${PREFIX}
