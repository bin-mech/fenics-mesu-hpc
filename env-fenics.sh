#!/bin/bash
#source ${HOME}/fenics-mesu-hpc/env-build-fenics.sh

module load intel/intel-mpi/2018.2
module load cmake/3.9

TAG=mesu-2019.2.0
BUILD_DIR=${HOME}/fenics-build-${TAG}
BUILD_THREADS=8
export PREFIX=${HOME}/fenics-${TAG}

# Note: These must be unset initially, need to code this in somehow.
export PETSC_DIR=${PREFIX}
export SLEPC_DIR=${PREFIX}
export PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig/:$PKG_CONFIG_PATH

export PATH=${PREFIX}/bin:${PATH}
export LD_LIBRARY_PATH=${PREFIX}/lib:${LD_LIBRARY_PATH}
export C_INCLUDE_PATH=${PREFIX}/include:${C_INCLUDE_PATH}
export CPLUS_INCLUDE_PATH=${PREFIX}/include:${CPLUS_INCLUDE_PATH}
export F_INCLUDE_PATH=${PREFIX}/include/petsc/finclude:${F_INCLUDE_PATH}

#export LD_LIBRARY_PATH=/usr/lib64/gcc/x86_64-suse-linux/6:$LD_LIBRARY_PATH

# For DOLFIN
export MPICC=mpicc
export MPICXX=mpicxx
export MPIFC=mpif90

export CFLAGS="-lpthread -lm -ldl -lstdc++"
export CXXFLAGS="-lpthread -lm -ldl -lstdc++"
export LDFLAGS="-lpthread -lm -ldl -lstdc++"

export CC=gcc-6
export CXX=g++-6
export FC=gfortran-6

# Bring in virtualenv with python package
#source $HOME/.local/bin/virtualenvwrapper.sh
#workon fenics-${TAG}

# Make sure OpenBLAS only uses one thread
export OPENBLAS_NUM_THREADS=1

export INSTANT_CACHE_DIR=${PREFIX}/.instant
export DIJITSO_CACHE_DIR=${PREFIX}/.dijitso
export INSTANT_ERROR_DIR=${PREFIX}/.instant

source $PREFIX/bin/activate
