
#PBS -S /bin/bash
#PBS -N FenicsRun
#PBS -o output.txt
#PBS -j oe
#PBS -q beta
#PBS -l walltime=00:20:00
#PBS -l select=1:ncpus=12



#load appropriate modules
module purge
#source activate my_root
source ${HOME}/fenics-mesu-hpc/env-fenics.sh

#move to PBS_O_WORKDIR
cd $PBS_O_WORKDIR
echo 'This job started on: ' `date`

mpirun -n 8 python3 demo_poisson.py
mpirun -n 12 python3 demo_block-assembly-3D2D.py 


