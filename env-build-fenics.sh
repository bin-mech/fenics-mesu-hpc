#!/bin/bash
# Setup environment variables and load modules
# for compiling FEniCS on HPCaVe cluster.


module purge

# Intel compilers
FENICS_TOOLCHAIN="intel"
module load intel/intel-mpi/2018.2
module load cmake/3.9

#module load boost/1.68
#module load petsc/3.10-impi
#module load slepc/3.10.0-impi
#module load eigen/3.25

FENICS_PYTHON=python3
TAG=mesu-2019.2.0
BUILD_DIR=${HOME}/fenics-build-${TAG}
BUILD_THREADS=3
export PREFIX=${HOME}/fenics-${TAG}


#export LD_LIBRARY_PATH=/opt/dev/Langs/Conda3/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/lib64/gcc/x86_64-suse-linux/6:$LD_LIBRARY_PATH

# For DOLFIN
export MPICC=mpicc
export MPICXX=mpicxx
export MPIFC=mpif90

export CFLAGS="-lpthread -lm -ldl -lstdc++"
export CXXFLAGS="-lpthread -lm -ldl -lstdc++"
export LDFLAGS="-lpthread -lm -ldl -lstdc++"

export CC=gcc-6
export CXX=g++-6
export FC=gfortran-6

# PREFIX is where the root of the install will be. $WORK or $HOME are good
# choices, or $STORE if your happy only to run FEniCS on gaia-80.  BUILD_DIR is
# where everything will be built. If you build on gaia-80, $STORE is a good
# choice.  Make sure you have created your /store directory at
# /store/rues/your-username on gaia-80 and set the variable $STORE in your
# .profile.


alias make="make -j${BUILD_THREADS}"
# Using gcc & g++ & gfortran version 6
mkdir -p ${PREFIX}/bin
ln -s -f /usr/bin/gcc-6  ${PREFIX}/bin/gcc
ln -s -f /usr/bin/g++-6  ${PREFIX}/bin/g++
ln -s -f /usr/bin/gfortran-6  ${PREFIX}/bin/gfortran

export PATH=${PREFIX}/bin:${PATH}
export LD_LIBRARY_PATH=${PREFIX}/lib:${LD_LIBRARY_PATH}
export C_INCLUDE_PATH=${PREFIX}/include:${C_INCLUDE_PATH}
export CPLUS_INCLUDE_PATH=${PREFIX}/include:${CPLUS_INCLUDE_PATH}
export F_INCLUDE_PATH=${PREFIX}/include/petsc/finclude:${F_INCLUDE_PATH}


FENICS_PYTHON_VERSION=$(${FENICS_PYTHON} -c 'import sys; print(str(sys.version_info[0]) + "." + str(sys.version_info[1]))')
FENICS_PYTHON_MAJOR_VERSION=$(${FENICS_PYTHON} -c 'import sys; print(str(sys.version_info[0]))')
FENICS_PYTHON_MINOR_VERSION=$(${FENICS_PYTHON} -c 'import sys; print(str(sys.version_info[1]))')
export PYTHONPATH=${PREFIX}/lib/python${FENICS_PYTHON_VERSION}/site-packages:${PYTHONPATH}

python3 -m venv $PREFIX
source $PREFIX/bin/activate