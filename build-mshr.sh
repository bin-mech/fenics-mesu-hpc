#!/bin/bash
source env-build-fenics.sh

export PETSC_DIR=${PREFIX}

#FENICS_VERSION="2017.2.0"
FENICS_VERSION="master"

# The ProgramOptions did not link on the Easybuild Boost, need to build Boost as well.
# Follow directions on boost.org with --prefix=$HOME/stow/boost before running script.
mkdir -p $BUILD_DIR

cd $BUILD_DIR && \
	if [ ! -d "mshr" ]; then
    	git clone https://bitbucket.org/fenics-project/mshr.git
	fi && \
    cd mshr && \
    git checkout ${FENICS_VERSION} && \
    mkdir -p build && \
    cd build && \
    cmake ../ -DCMAKE_BUILD_TYPE=Release -DBOOST_ROOT=${PREFIX} -DCMAKE_INSTALL_PREFIX=${PREFIX} -DMSHR_ENABLE_VTK=True && \
    make -j ${BUILD_THREADS} && \
    make install
    cd ../python
    python setup.py install --prefix=$PREFIX
