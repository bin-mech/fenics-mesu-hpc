#!/bin/bash
# chmod u+x build-pybind11.sh
source env-build-fenics.sh

PYBIND_VERSION=2.2.3

mkdir -p $BUILD_DIR

cd ${BUILD_DIR} && \
  wget --read-timeout=10 -nc https://github.com/pybind/pybind11/archive/v${PYBIND_VERSION}.tar.gz && \
  mkdir -p ${BUILD_DIR}/pybind && \
  tar -xf v${PYBIND_VERSION}.tar.gz -C ${BUILD_DIR}/pybind --strip-components=1 && \
  cd pybind && \ 
  mkdir -p build && \
  cd build
  cmake -DPYBIND11_TEST=off -DCMAKE_INSTALL_PREFIX=${PREFIX} ../ && \
  make install
  
cd ${BUILD_DIR} && \  
  export PYBIND11_DIR=${PREFIX} && \
  pip3 -v install pybind11 --user && \
  cd pybind &&\
  python3 setup.py build && \
  python3 setup.py build --debug