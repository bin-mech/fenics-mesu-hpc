source env-build-fenics.sh

SLEPC_VERSION="3.12.2"

export PETSC_DIR=${PREFIX}
unset SLEPC_DIR
mkdir -p ${BUILD_DIR}

cd ${BUILD_DIR} && \
   wget --read-timeout=10 -nc http://slepc.upv.es/download/distrib/slepc-${SLEPC_VERSION}.tar.gz -O slepc-${SLEPC_VERSION}.tar.gz && \
   tar -xf slepc-${SLEPC_VERSION}.tar.gz && \
   cd slepc-${SLEPC_VERSION} && \
   python2 ./configure --prefix=${PREFIX} && \
   make MAKE_NP=${BUILD_THREADS} && \
   make install
