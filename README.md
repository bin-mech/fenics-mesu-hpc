# fenics-mesu-hpc 

## Scripts to build FEniCS on MESU in UPMC MESU HPC  

These scripts (main based on [fenics-gaia-cluster](https://bitbucket.org/unilucompmech/fenics-gaia-cluster)) will 
automatically build the latest stable version of [FEniCS](http://fenicsproject.org) with PETSc and SLEPc 
support on [UPMC-Sorbonne UniversitÚs High Performance Computing platform MESU](http://hpcave.upmc.fr).

## Compiling instructions 

First clone this repository.
```
#!shell
$ cd $HOME
$ git clone https://bin-mech@bitbucket.org/bin-mech/fenics-mesu-hpc.git
$ cd fenics-mesu-hpc
```

Run the following command to build:
```
#!shell
$ ./build-all.sh | tee build.log
```

Wait for the build to finish. The output of the build will be stored in build.log 
as well as outputted to the screen. 


When you want to run FEniCS you must reserve 
resources on the cluster and then setup your environment using:
```
#!shell
$ source env-fenics.sh
```
before running any scripts.

## Advanced 

You can adjust the build location and the installation location in the file
`env-build-fenics.sh` Also you can adjust the location of your Instant JIT
cache using the variables in the file `env-fenics.sh`.

## Running FEniCS MPI jobs 

Runing the very simple demo-poisson.py test on the front end to check the compilation is successful or not 

```
#!shell
$ cd $HOME
$ cd fenics-mesu-hpc
$ source env-fenics.sh
$ python3 demo-poisson.py
```
### Set up the job script and submit 

PLEASE check [GET STARTED](http://hpcave.upmc.fr/index.php/usage/get-started/).

## Comments or questions 

Send a mail to Bin LI or [Corrado MAURINI](http://www.lmm.jussieu.fr/~corrado/)

* `bin.li@upmc.fr` 
* `corrado.maurini@upmc.fr`

## Read, write and convert different unstructured meshes formats 

Simply call
```
meshio-convert input.msh output.vtu
```
with any of the supported formats.

Please check [MESHIO](https://github.com/nschloe/meshio) for complete information.
