#!/bin/bash
source env-build-fenics.sh

VERSION="3.12.2"

mkdir -p $BUILD_DIR
cd ${BUILD_DIR} && \
   wget --read-timeout=10 -nc http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-${VERSION}.tar.gz
   tar -xf petsc-${VERSION}.tar.gz && \
   cd petsc-${VERSION} && \
   python2 ./configure --with-cc="mpicc" \
               --with-fc="mpif90"\
               --with-cxx="mpicxx" \
               --COPTFLAGS="-O2 -mtune=intel" \
               --CXXOPTFLAGS="-O2 -mtune=intel" \
               --FOPTFLAGS="-O2 -mtune=intel" \
               --CFLAGS=${CFLAGS} \
               --CXXFLAGS=${CXXFLAGS} \
               --LDFLAGS=${LDFLAGS} \
               --with-blas-lapack-lib=/usr/lib64/libopenblas.so \
               --download-metis \
               --download-parmetis \
               --download-scotch \
               --download-suitesparse \
               --download-scalapack \
               --download-hypre \
               --download-mumps \
               --download-ml \
               --with-debugging=0 \
               --with-shared-libraries \
               --download-slepc \
               --prefix=${PREFIX} && \
    make MAKE_NP=${BUILD_THREADS} && \
    make install 

